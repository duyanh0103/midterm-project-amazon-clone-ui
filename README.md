# midterm project Amazon clone UI



## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/duyanh0103/midterm-project-amazon-clone-ui.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/duyanh0103/midterm-project-amazon-clone-ui/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Automatically merge when pipeline succeeds](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!).  Thank you to [makeareadme.com](https://www.makeareadme.com/) for this template.

## Suggestions for a good README
Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.

## Name
Choose a self-explaining name for your project.

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.

---
## what I have done for the midterm project:

# 1.  **Login screen:** 

![Screenshot_1653712102](https://user-images.githubusercontent.com/90690747/170810198-f3f662ae-f051-44d8-be11-f5b81eeb58dd.png)

---

## Beside it can validate when user let the input empty

![validate](https://user-images.githubusercontent.com/90690747/170810326-2824c43a-82a0-4ec0-98b8-1e6713e55752.png)

---

# 2. **Sign in Screen:** 

![signup](https://user-images.githubusercontent.com/90690747/170810282-67f60788-1df3-4ff8-a423-535dc3a6a240.png)

## when we click the sign up button it will move back to the Login

---

# 3.  **Home page:**

![homepage](https://user-images.githubusercontent.com/90690747/170810354-ab838026-b0b8-4346-ba7d-89d1d9c7aea0.png)

---

# 4. **Expand screen:**

## when we click the see all orange button 

![expand screen](https://user-images.githubusercontent.com/90690747/170810399-bbaf6409-d536-48f7-8bcd-5f855985f0d6.png)

---

# 5.  **Description book:**

## When we click in a certain item which is book it will display the description

![description](https://user-images.githubusercontent.com/90690747/170810458-6affd532-56a2-48ae-a8d1-f028b1136527.png)


## Moreover, it have a way to show favorites book like the heart can change the color at the top of the screen

![not favorite](https://user-images.githubusercontent.com/90690747/170810548-0f174e28-4463-41d0-9317-d735fce27931.png)


## At the homepage there is a list of best sellers books and it can scroll horizontally:

![scroll horizontal](https://user-images.githubusercontent.com/90690747/170810722-4ce8a8a1-a914-43aa-8fc6-1efb4a529f6b.png)

# 6. **New releases:**

## scroll down the page there is a new release:

![new release](https://user-images.githubusercontent.com/90690747/170810636-fb867a72-5bdc-41f0-bdec-225296cc27f3.png)

# function is similar with the description and favorite.

# 7. **floating action button:**

## Display the list of catergories.

![fba](https://user-images.githubusercontent.com/90690747/170810845-06a88965-6915-48ab-a83b-b3823aa9260c.png)