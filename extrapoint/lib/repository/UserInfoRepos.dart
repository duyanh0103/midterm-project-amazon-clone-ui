abstract class UserInfoRepos{
  String getName();
  String getPhone();
  String getAddress();
  void setName(String name);
  void setPhone(String phone);
  void setAddress(String address);
  void setAllData(String address, String name, String phone);
}