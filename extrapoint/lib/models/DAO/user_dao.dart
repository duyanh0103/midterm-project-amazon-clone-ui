import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/foundation.dart';

class UserDao extends ChangeNotifier{
  final auth = FirebaseAuth.instance;

  Future<bool> isLoggedIn(){
    return Future.value(auth.currentUser !=null);
  }

  String? userId(){
    return auth.currentUser?.uid;
  }

  String? email(){
    return auth.currentUser?.email;
  }

  //SIGN UP
  Future<String> signup(String email, String password) async{
    try{
      await auth.createUserWithEmailAndPassword(email: email, password: password);
      await auth.signOut();
      notifyListeners();
      return "";
    } on FirebaseAuthException catch (e){
      if(e.code == 'weak-password'){
        return "Mật khẩu yếu";
      }
      else if(e.code == 'email-already-in-use'){
        return "Tài khoản đã tồn tại";
      }
    }
    catch(e){
      print(e);
    }
    return "Lỗi khi đăng ký tài khoản";
  }

  // SIGN IN
  Future<String> login(String email, String password) async{
    try{
      await auth.signInWithEmailAndPassword(email: email, password: password);
      notifyListeners();
      return "";
    }
    on FirebaseAuthException catch (e){
      if(e.code == 'weak-password'){
        return "Mật khẩu yếu";
      }
      else if (e.code == 'wrong-password') {
        return "Sai tài khoản hoặc mật khẩu";
      }
    }
    catch(e){
      print(e);
    }
    return "Đăng nhập thất bại";
  }

  //LOG OUT
  void logout() async{
    await auth.signOut();
    notifyListeners();
  }

}