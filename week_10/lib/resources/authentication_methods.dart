import 'package:firebase_auth/firebase_auth.dart';

class AuthenticationMethods {
  FirebaseAuth firebaseAuth = FirebaseAuth.instance;
  //dùng future vì sẽ làm việc với services các kiểu nên cần có async trả về string để biết là successful/failure
  //======================= Sign Up =========================
  Future<String> SignUpUser({
    required String name,
    required String address,
    required String email,
    required String password,
  }) async {
    //xóa space bằng trim()
    name.trim();
    address.trim();
    email.trim();
    password.trim();
    String output = "Something is wrong";
    if (name.isNotEmpty &&
        address.isNotEmpty &&
        email.isNotEmpty &&
        password.isNotEmpty) {
      //func
      try {
        await firebaseAuth.createUserWithEmailAndPassword(
            email: email, password: password);
            output = "success";
      } on FirebaseAuthException catch (e) {
        output = e.message.toString(); //dùng cách này để không hiện thị lỗi thô dành cho dev 
      }
      
    } else {
     output = "Please enter all the fields";
    }
    return output;
  }
  //================== Sign In ========================
    Future<String> SignInUser({
    required String email,
    required String password,
  }) async {
    //xóa space bằng trim()
    email.trim();
    password.trim();
    String output = "Something is wrong";
    if (email.isNotEmpty &&
        password.isNotEmpty) {
      //func
      try {
        await firebaseAuth.signInWithEmailAndPassword(
            email: email, password: password);
            output = "success";
      } on FirebaseAuthException catch (e) {
        output = e.message.toString(); //dùng cách này để không hiện thị lỗi thô dành cho dev 
      }
      
    } else {
     output = "Please enter all the fields";
    }
    return output;
  }
}
