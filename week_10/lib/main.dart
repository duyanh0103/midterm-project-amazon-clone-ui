import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:week_10/screens/sign_in_screen.dart';

void main() async {
  WidgetsFlutterBinding
      .ensureInitialized(); // chắc chắn khởi tạo nếu k bị null thì sẽ k hiện gì lên
  // nếu nó mở dạng web
  if (kIsWeb) {
    await Firebase.initializeApp(
        options: const FirebaseOptions(
            apiKey: "AIzaSyAohFUfoojyyDKqmy5dQr0JlItxYo79d7M",
            authDomain: "clone-bcb7b.firebaseapp.com",
            projectId: "clone-bcb7b",
            storageBucket: "clone-bcb7b.appspot.com",
            messagingSenderId: "987007610381",
            appId: "1:987007610381:web:0b1786fa5684fe2806ef44"));
  } else {
    // Android
    await Firebase.initializeApp();
  }
  runApp(const AmazonClone());
}

class AmazonClone extends StatelessWidget {
  const AmazonClone({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Amazon Clone',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      // mỗi lần user signIn hay signOut cái widget này sẽ rebuild
      home: StreamBuilder(
        stream: FirebaseAuth.instance.authStateChanges(),
        builder: (context, AsyncSnapshot<User?> user) {
          // nếu đang trong qtr fetch data
          //có gì đó thay đổi thì cái widget này rebuild và cái async đc chạy trong firebase
          if (user.connectionState == ConnectionState.waiting) {
            return const Center(
              child: CircularProgressIndicator(color: Colors.orange),
            );
          } //nếu đã đăng nhập r thì lần sau sẽ tự vào
          else if (user.hasData) {
            return Scaffold(
              body: Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    const Text("Đã đăng nhập"),
                    IconButton(
                        onPressed: () async {
                          FirebaseAuth.instance.signOut();
                        },
                        icon: const Icon(Icons.logout)),
                  ],
                ),
              ),
            );
          } else {
            return const SignInScreen();
          }
        },
      ),
    );
  }
}
