import 'package:flutter/material.dart';

class Utils {
  Size getScreenSize() {
    // ta cần context cho mediaQuerry thay vì nhét thẳng vào para thì dùng thẳng mediaQuerry data
    return MediaQueryData.fromWindow(WidgetsBinding.instance!.window).size;
  }

  showSnackBar({required BuildContext context, required String content}) {
    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        // chỉnh style snack bar
        backgroundColor: Colors.red,
        shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(10), topRight: Radius.circular(10))),
        content: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              content,
              style: const TextStyle(
                color: Colors.white,
                
              ),
            )
          ],
        ),
      ),
    );
  }
}
