import 'package:amazon_ui_clone/pages/home/home_page.dart';
import 'package:flutter/material.dart';
import 'pages/login_screen/loginScreen.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      routes: {
        //key: đặt tên '/' là root name
        '/': (context) => LoginScreen(),
        '/login': (context) => LoginScreen(),
        //để tạm nha xíu xóa :)
        //  (context) => feedSCreen(),
        HomePage.route: (context) => HomePage()
      },
      initialRoute: '/',
    );
  }
}
