class BookModel {
  String image;
  String secondImage;
  String title;
  String subtitle;
  String description;
  double price;
  bool favorite;

//constructor
  BookModel({
    required this.image,
    required this.secondImage,
    required this.title,
    required this.subtitle,
    required this.description,
    required this.price,
    required this.favorite,
  });

  //new book
  static BookModel newBook = BookModel(
    image: "assets/images/new_book_2_cover.png",
    secondImage: "assets/images/new_book_2.jpeg",
    title: "It's Mine",
    subtitle: "LuckS",
    description:
        "There’s nothing that Yo-Han Do doesn’t know about his crush, Da-Jeong. Where she sits in class, works after school... and even where she lives. But unable to approach her, he watches her every move from afar and will do anything to protect her. Anything. \n\n'What can I do to get closer to you?' Today, just like yesterday, Yo-Han Do is watching.",
    price: 200000,
    favorite: true,
  );

  //create list of books
  static List<BookModel> books = [
    BookModel(
      image: "assets/images/book_1_limited.jpeg",
      secondImage: "assets/images/book_1_2nd.jpeg",
      title: "Jujutsu Kaisen Vol 0 (Limited)",
      subtitle: "Gege Akutami",
      description:
          "Yuta Okkotsu, a high schooler who gains control of an extremely powerful Cursed Spirit and gets enrolled in the Tokyo Prefectural Jujutsu High School by Jujutsu Sorcerers to help him control his power and keep an eye on him.",
      price: 500000,
      favorite: true,
    ),
    BookModel(
      image: "assets/images/book_2.jpg",
      secondImage: "assets/images/book_1_2nd.jpeg",
      title: "Jujutsu Kaisen Vol 0",
      subtitle: "Gege Akutami",
      description:
          "Yuta Okkotsu, a high schooler who gains control of an extremely powerful Cursed Spirit and gets enrolled in the Tokyo Prefectural Jujutsu High School by Jujutsu Sorcerers to help him control his power and keep an eye on him.",
      price: 50000,
      favorite: true,
    ),
    BookModel(
      image: "assets/images/book_3.jpeg",
      secondImage: "assets/images/book_3.jpeg",
      title: "Ngày Xưa có một chuyện tình",
      subtitle: "Nguyễn Nhật Ánh",
      description: "Ngày xưa có một chuyện tình là một câu chuyện cảm động khi người ta yêu nhau, nỗi khát khao một hạnh phúc êm đềm ấm áp đến thế; hay đơn giản chỉ là chuyện ba người - anh, em, và người ấy...?",
      price: 30000,
      favorite: false,
    ),
    BookModel(
      image: "assets/images/book_4.jpg",
      secondImage: "assets/images/book_4.jpg",
      title: "Tôi thấy hoa vàng trên cỏ xanh",
      subtitle: "Nguyên Nhật Ánh",
      description: "Nội dung truyện nói vè tuổi thơ nghèo khó của hai anh em Thiều và Tường ở một làng quê Việt thân thuộc và nên thơ. Là nơi đã chứng kiến những rung động đầu đời của cả hai, tình cảm gia đình, tình anh em yêu thương chân thành, cũng như những đố kỵ, ghen tuông và những nỗi đau trong veo trong quá trình trưởng thành.",
      price: 45000,
      favorite: false,
    ),
  ];
}
