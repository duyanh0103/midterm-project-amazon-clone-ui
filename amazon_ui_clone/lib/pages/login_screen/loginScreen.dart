// TODO Implement this library.
import 'package:amazon_ui_clone/pages/home/home_page.dart';
import 'package:amazon_ui_clone/pages/login_screen/signUp_screen.dart';
import 'package:amazon_ui_clone/validator/loginValidator.dart';
import 'package:flutter/material.dart';

class LoginScreen extends StatefulWidget {
  static const route = '/login';

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return LoginScreenState();
  }
}

class LoginScreenState extends State<StatefulWidget> with LoginValidator {
  final formKey = GlobalKey<FormState>();
  final emailController = TextEditingController();
  final passwordController = TextEditingController();
  // lam progressive circle to make it look like loading
  bool _isLoading = false;
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: Container(
          height: MediaQuery.of(context).size.height,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              // chỉnh logo ins thay đổi vi tri
              // Flexible(
              //   child: Container(),
              //   flex: 2,
              // ),
              // svg img
              Image.asset(
                'assets/images/amazon.png',
                height: 64,
              ),
              const SizedBox(
                height: 40,
              ),
              Form(
                key: formKey,
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.symmetric(
                        horizontal: 30,
                        vertical: 10,
                      ),
                      child: emailField(),
                    ),
                    Padding(
                      padding:
                          EdgeInsets.symmetric(horizontal: 30, vertical: 10),
                      child: passwordField(),
                    ),
                    SizedBox(height: 20),
                    InkWell(
                      onTap: validate,
                      child: Container(
                        width: 250,
                        //loading
                        child: _isLoading
                            ? const Center(
                                child: CircularProgressIndicator(
                                  color: Colors.white,
                                ),
                              )
                            : const Text(
                                "Login",
                                style: TextStyle(
                                  color: Colors.black,
                                ),
                              ),
                        // width: double.infinity, //maximum posible value
                        alignment: Alignment.center,
                        padding: const EdgeInsets.symmetric(vertical: 12),
                        decoration: const BoxDecoration(
                          borderRadius: BorderRadius.all(
                            Radius.circular(4),
                          ),
                          color: Colors.amberAccent,
                          boxShadow: [
                            BoxShadow(
                              color: Colors.black87,
                              offset: Offset(5, 5),
                              blurRadius: 9.0,
                              spreadRadius: 1.0,
                            ),
                            BoxShadow(
                              color: Colors.white,
                              offset: const Offset(0, 0),
                              blurRadius: 0.0,
                              spreadRadius: 0.0,
                            ),
                          ],
                        ),
                      ),
                    ),
                    Flexible(
                      child: Container(),
                      flex: 2,
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 30,
              ),
              //signupText()
              Row(
                children: [
                  Expanded(
                    child: Container(
                      height: 1,
                      color: Colors.grey,
                    ),
                  ),
                  const Padding(
                    padding: EdgeInsets.symmetric(horizontal: 10),
                    child: Text(
                      "New to Amazon?",
                      style: TextStyle(color: Colors.grey),
                    ),
                  ),
                  Expanded(
                    child: Container(
                      height: 1,
                      color: Colors.grey,
                    ),
                  ),
                ],
              ),

              const SizedBox(
                height: 30,
              ),
              Container(
                width: 250,
                child: signUpButton(),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget emailField() {
    return TextFormField(
      keyboardType: TextInputType.emailAddress,
      controller: emailController,
      decoration: const InputDecoration(labelText: 'Email address'),
      validator: validateEmail,
    );
  }

  Widget passwordField() {
    return TextFormField(
      controller: passwordController,
      obscureText: true,
      decoration: const InputDecoration(labelText: 'Password'),
      validator: validatePassword,
    );
  }

  Widget signUpButton() {
    return InkWell(
      child: Container(
        padding: EdgeInsets.all(8),
        child: const Text(
          "Create an Amazon Account",
          style: TextStyle(
            letterSpacing: 0.6,
            color: Colors.black,
          ),
          textAlign: TextAlign.center,
        ),
        color: Colors.grey[400],
      ),
      onTap: () {
        Navigator.pushReplacement(
          context,
          MaterialPageRoute(
            builder: (context) {
              return const SignUpScreen();
            },
          ),
        );
      },
    );
  }

// Widget signupText(){}

  void validate() {
    final form = formKey.currentState;
    if (!form!.validate()) {
      return;
    } else {
      final email = emailController.text;
      final password = passwordController.text;

      Navigator.of(context)
          .pushReplacementNamed(HomePage.route, arguments: email);

      setState(() {});
    }
  }
}
