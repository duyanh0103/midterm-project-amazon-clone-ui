import 'package:amazon_ui_clone/constants.dart';
import 'package:amazon_ui_clone/models/books_model.dart';
import 'package:amazon_ui_clone/pages/home/book/widget/book_display.dart';
import 'package:amazon_ui_clone/pages/home/book/widget/description.dart';
import 'package:flutter/material.dart';

class BookPage extends StatefulWidget {
  // BookPage({Key? key}) : super(key: key);
  final BookModel book;
  BookPage({required this.book});

  @override
  State<BookPage> createState() => _BookPageState();
}

class _BookPageState extends State<BookPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
        leading: IconButton(
          onPressed: () {
            Navigator.pop(context);
          },
          icon: Icon(
            Icons.keyboard_arrow_left_outlined,
            color: Colors.black87,
          ),
        ),
        actions: [
          Padding(
            padding: EdgeInsets.all(Constants.kPadding),
            child: IconButton(
              onPressed: () {
                //TODO
                setState(() {
                  widget.book.favorite = !widget.book.favorite;
                });
              },
              icon: Icon(
                widget.book.favorite? Icons.favorite: Icons.favorite_border,
                color: Colors.red,
              ),
            ),
          ),
        ],
      ),
      //************body*****************/
      //TODO
      body: SafeArea(
        child: Container(
          height: double.infinity,
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: Constants.kPadding * 4),
            child: Column(
              children: [
                //TODO
                //book-display
                ...buildBookDisplay(book: widget.book),
                ...buildDescription(book: widget.book),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
