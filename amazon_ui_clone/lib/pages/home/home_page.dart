import 'package:amazon_ui_clone/pages/home/widgets/appbar.dart';
import 'package:amazon_ui_clone/pages/home/widgets/books_list.dart';
import 'package:amazon_ui_clone/pages/home/widgets/fba.dart';
import 'package:amazon_ui_clone/pages/home/widgets/icons_list.dart';
import 'package:amazon_ui_clone/pages/home/widgets/navigation_bar.dart';
import 'package:amazon_ui_clone/pages/home/widgets/new_book.dart';
import 'package:amazon_ui_clone/pages/home/widgets/search_bar.dart';
import 'package:flutter/material.dart';
import 'package:amazon_ui_clone/pages/home/widgets/header.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);
  static const route = '/homepage';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      //**********appBar************
      appBar: buildAppBar(),
      //**********body**************
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            ...buildHeader(),
            buildSearchBar(),
            buildIconList(),
            buildBookList(),
            buildNewBook(),
          ],
        ),
      ),
      //********bottomNavigationBar**************
      extendBody: true,
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      floatingActionButton: BuildFBA(),
      bottomNavigationBar: buildNavigationBar(),
    );
  }
}
