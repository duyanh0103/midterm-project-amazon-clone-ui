import 'package:amazon_ui_clone/constants.dart';
import 'package:amazon_ui_clone/models/icons_model.dart';
import 'package:flutter/material.dart';

List<IconModel> headerImages =
    IconModel.icons; //create the models for headerImages

Widget buildIconList() => Padding(
      padding: EdgeInsets.only(bottom: Constants.kPadding),
      child: SingleChildScrollView(
        //trượt ngang
        scrollDirection: Axis.horizontal,
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: List.generate(
            headerImages.length,
            (index) => SizedBox(
              width: 100,
              child: Column(
                children: [
                  //icon
                  Image.asset(
                    headerImages[index].icon,
                    height: 50,
                    width: 50,
                  ),
                  //title
                  Text(
                    headerImages[index].title,
                    textAlign: TextAlign.center,
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
