import 'package:amazon_ui_clone/constants.dart';
import 'package:amazon_ui_clone/models/books_model.dart';
import 'package:amazon_ui_clone/pages/home/book/book_page.dart';
import 'package:amazon_ui_clone/pages/home/widgets/books_list.dart';
import 'package:flutter/material.dart';



class BookExpand extends StatefulWidget {
  BookExpand({Key? key}) : super(key: key);
  List<BookModel> books = BookModel.books; // create this model books_model

  @override
  State<BookExpand> createState() => _BookExpandState();
}

class _BookExpandState extends State<BookExpand> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
        leading: IconButton(
          onPressed: () {
            Navigator.pop(context);
          },
          icon: Icon(
            Icons.keyboard_arrow_left_outlined,
            color: Colors.black87,
          ),
        ),
      ),
      //***********body*****************/
      body: SafeArea(
        child: Container(
          height: double.infinity,
          child: SingleChildScrollView(
            scrollDirection: Axis.vertical,
            child: Column(
              // mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: List.generate(
                books.length,
                (index) => Padding(
                  padding: EdgeInsets.only(
                    bottom: Constants.kPadding * 2,
                    // right: Constants.kPadding,
                    //nếu left ==0  thì Constants.kPadding = 0;
                    left: index == 0 ? Constants.kPadding : 0,
                  ),
                  child: InkWell(
                    onTap: () {
                      //TO DO:
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => BookPage(
                            //TODO:
                            //to add later
                            book: books[index],
                          ),
                        ),
                      );
                    },
                    child: Row(
                      children: [
                        Card(
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10.0),
                          ),
                          elevation: 4,
                          child: ClipRRect(
                            borderRadius: BorderRadius.circular(10.0),
                            child: Image.asset(
                              books[index].image,
                              height: 180,
                              width: 120,
                              fit: BoxFit.cover,
                            ),
                          ),
                        ),
                        //container chứa tên (title, subtitle, prices)
                        Column(
                          children: [
                            Container(
                              width: 120,
                              child: Text(
                                books[index].title,
                                style: const TextStyle(
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ),
                            Container(
                              width: 120,
                              child: Text(
                                books[index].subtitle,
                                style: const TextStyle(
                                  fontSize: 10,
                                  color: Colors.blueGrey,
                                ),
                              ),
                            ),
                          ],
                        ),
                        Padding(
                          padding: EdgeInsets.only(left: 30),
                          child:Container(
                            width: 120,
                            child: Text(
                              books[index].price.toString() + ' VND',
                              style: const TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 13,
                                color: Color.fromARGB(255, 24, 145, 245),
                              ),
                              textAlign: TextAlign.end,
                            ),
                            
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ), //khởi tạo ở trên và trong model
            ),
          ),
        ),
      ),
    );
  }
}
