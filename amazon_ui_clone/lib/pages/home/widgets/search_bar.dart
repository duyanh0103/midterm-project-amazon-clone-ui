import 'package:amazon_ui_clone/constants.dart';
import 'package:flutter/material.dart';

Widget buildSearchBar() => Padding(
      padding: EdgeInsets.all(2 * Constants.kPadding),
      child: TextField(
        decoration: InputDecoration(
          //search bar with text and color
          hintText: "Search a book",
          fillColor: Colors.black.withOpacity(0.1),
          filled: true,
          //icon search
          prefixIcon: Icon(Icons.search),
          //contentPadding
          contentPadding: EdgeInsets.symmetric(
            vertical: 0.0,
            horizontal: Constants.kPadding,
          ),
          //border
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(20),
            borderSide: BorderSide.none,
          ),
        ),
      ),
    );


