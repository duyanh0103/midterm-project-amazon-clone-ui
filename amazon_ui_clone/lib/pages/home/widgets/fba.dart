import 'package:amazon_ui_clone/constants.dart';
import 'package:flutter/material.dart';

class BuildFBA extends StatefulWidget {
  BuildFBA({Key? key}) : super(key: key);

  @override
  State<BuildFBA> createState() => _BuildFBAState();
}

class _BuildFBAState extends State<BuildFBA> {
  @override
  Widget build(BuildContext context) {
    return FloatingActionButton(
      onPressed: () {
        showModalBottomSheet(
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
          context: context,
          builder: (context) => Padding(
            padding: const EdgeInsets.symmetric(
              horizontal: 40,
            ),
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(
                    height: 40.0,
                  ),
                  Text(
                    'Shop by',
                    style: TextStyle(
                      color: Colors.black54,
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        'Category',
                        style: TextStyle(
                          fontSize: 25,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      IconButton(
                        onPressed: () {},
                        icon: Icon(Icons.search),
                        color: Colors.deepOrange,
                      ),
                    ],
                  ),
                  SizedBox(
                    height: Constants.kPadding,
                  ),
                  //create widget because of using many times
                  _buildCategory(
                    category: "Books",
                    iconData: Icons.book,
                    color: Colors.deepOrange,
                  ),
                  _buildCategory(
                    category: "Music & Movies",
                    iconData: Icons.movie_creation,
                    color: Colors.amber,
                  ),
                  _buildCategory(
                    category: "games",
                    iconData: Icons.sports_esports,
                    color: Colors.red,
                  ),
                  _buildCategory(
                    category: "Grocery",
                    iconData: Icons.local_grocery_store,
                    color: Colors.green,
                  ),
                  _buildCategory(
                    category: "Health & Beauty",
                    iconData: Icons.healing,
                    color: Colors.blue,
                  ),
                  _buildCategory(
                    category: "Sport",
                    iconData: Icons.sports_basketball,
                    color: Colors.grey,
                  ),
                  _buildCategory(
                    category: "Technical",
                    iconData: Icons.mediation,
                    color: Colors.indigo,
                  ),
                  const SizedBox(
                    height: 40.0,
                  )
                ],
              ),
            ),
          ),
        );
      },
      backgroundColor: Colors.white,
      child: Icon(
        Icons.list_alt_rounded,
        color: Colors.blueAccent,
      ),
    );
  }

//create widget for Category
  Widget _buildCategory({
    required String category,
    required IconData iconData,
    required Color color,
  }) {
    return Padding(
      padding: EdgeInsets.symmetric(
        vertical: Constants.kPadding,
      ),
      child: InkWell(
        onTap: () {},
        borderRadius: BorderRadius.circular(25),
        child: Container(
          width: double.infinity,
          child: Wrap(
            crossAxisAlignment: WrapCrossAlignment.center,
            spacing: 2 * Constants.kPadding,
            children: [
              CircleAvatar(
                backgroundColor: color,
                child: Icon(
                  iconData,
                  color: Colors.white,
                ),
              ),
              Text(
                category,
                style: TextStyle(
                  fontWeight: FontWeight.w600,
                  fontSize: 20,
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
