import 'package:flutter/material.dart';
import 'package:amazon_ui_clone/constants.dart';

List<Widget> buildHeader() => [
      Padding(
        padding: EdgeInsets.only(
          left: 2 * Constants.kPadding,
          top: Constants.kPadding,
        ),
        child: const Text(
          "Bookshelf",
          style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 25,
          ),
        ),
      ),
      Padding(
        padding: EdgeInsets.symmetric(
          horizontal: 2 * Constants.kPadding,
          vertical: Constants.kPadding,
        ),
        child: const Text(
          "Welcome to the bookshelf",
          style: TextStyle(
            color: Colors.black54,
          ),
        ),
      ),
    ];