import 'package:amazon_ui_clone/constants.dart';
import 'package:amazon_ui_clone/models/books_model.dart';
import 'package:amazon_ui_clone/pages/home/book/book_page.dart';
import 'package:flutter/material.dart';

final newBook = BookModel.newBook; //create newbook

class buildNewBook extends StatelessWidget {
  const buildNewBook({Key? key}) : super(key: key);
  //need the context of the new book
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Padding(
          padding: EdgeInsets.symmetric(
            vertical: Constants.kPadding,
            horizontal: 2 * Constants.kPadding,
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                "Hot New Releases",
                style: TextStyle(
                  color: Colors.black,
                  fontWeight: FontWeight.bold,
                  fontSize: 15,
                ),
              ),
              //icon button
              IconButton(
                onPressed: () {
                  //TO DO:
                }, //xíu xét sau
                icon: const Icon(Icons.arrow_forward_ios_outlined),
              ),
            ],
          ),
        ),

        ///*********************************NEW BOOK*************************************** */
        InkWell(
          onTap: () {
            //TO DO:
            //trigger later
            Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => BookPage(
                        //TODO:
                        //to add later
                        book: newBook,
                      ),
                    ),
                  );
          },
          child: Column(
            children: [
              Padding(
                padding: EdgeInsets.symmetric(horizontal: Constants.kPadding),
                child: Card(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10.0),
                  ),
                  elevation: 4,
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(10.0),
                    child: Image.asset(
                      newBook.image,
                      height: 180,
                      width: double.infinity,
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
              ),
              //container chứa tên (title, subtitle, prices)
              Padding(
                padding:
                    EdgeInsets.symmetric(horizontal: 2 * Constants.kPadding),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          newBook.title,
                          style: const TextStyle(
                            fontSize: 16,
                            color: Colors.black,
                          ),
                        ),
                        Text(
                          newBook.subtitle,
                          style: const TextStyle(
                            fontSize: 10,
                            color: Colors.blueGrey,
                          ),
                          textAlign: TextAlign.left,
                        ),
                        Text(
                          newBook.price.toString() + ' VND',
                          style: const TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 13,
                            color: Color.fromARGB(255, 24, 145, 245),
                          ),
                        ),
                      ],
                    ),
                    TextButton(
                      onPressed: () {},
                      child: const Text(
                        '7.4',
                      ),
                      style: TextButton.styleFrom(
                        padding: EdgeInsets.symmetric(
                          vertical: 2.0,
                          horizontal: Constants.kPadding,
                        ),
                        backgroundColor: Colors.deepOrange,
                        primary: Colors.white,
                        minimumSize: Size(5, 5),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(20.0)),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),

        //Certain Spacing
        //TO DO:
        SizedBox(
          height: MediaQuery.of(context).padding.bottom,
        )
      ],
    );
  }
}
