import 'package:flutter/material.dart';

Widget buildNavigationBar() => BottomAppBar(
      //gắn fba vào bottom app bar
      shape: CircularNotchedRectangle(),
      notchMargin: 8.0,
      //trong appbar
      child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
        Padding(
          padding: const EdgeInsets.only(
            left: 50,
          ),
          child: IconButton(
            onPressed: () {},
            icon: const Icon(Icons.send_rounded),
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(
            right: 50,
          ),
          child: IconButton(
            onPressed: () {},
            icon: const Icon(
              Icons.add_shopping_cart,
            ),
          ),
        ),
      ]),
    );
